# Changelog
All notable changes to this project will be documented in this file.

## [Master] 

## [1.1.5] - 2022-12-09
* [Issue #32](https://gitlab.com/axual/public/axual-client-python/-/issues/32) AxualConsumer#assign([]) should unassign

## [1.1.4] - 2022-06-21
### Changes
* [Issue #31](https://gitlab.com/axual/public/axual-client-python/-/issues/31) Commit method of consumer class does not unresolve topic names in return value

## [1.1.3] - 2022-05-31
### Changes
* Updated version.py

## [1.1.2] - 2022-05-19
### Changes
* Consumer and DeserializingConsumer now inherit from _AxualConsumer, which implements the common logic of consumers
* Producer and SerializingProducer now inherit from _AxualProducer, which implements the common logic of producers
* [Issue #14](https://gitlab.com/axual/public/axual-client-python/-/issues/14) Introduced String integration tests to run against standalone in gitlab pipeline
* [Issue #25](https://gitlab.com/axual/public/axual-client-python/-/issues/25) Added integration tests for AVRO producer and consumer
* [Issue #26](https://gitlab.com/axual/public/axual-client-python/-/issues/26) Fixed an issue where the discovery service, once running out of clients, would not restart when new clients registered
* [Issue #27](https://gitlab.com/axual/public/axual-client-python/-/issues/27) Fixed an issue where calling list_topic with no arguments in the producer resulted in an error
* [Issue #28](https://gitlab.com/axual/public/axual-client-python/-/issues/28) Fixed inconsistent resolution of topic names in the clients
* [Issue #29](https://gitlab.com/axual/public/axual-client-python/-/issues/29) Fixed TypeError and ValueError happening when calling the store_offsets method of the consumer class
* [Issue #30](https://gitlab.com/axual/public/axual-client-python/-/issues/30) Fixed race condition in the discovery initialisation

## [1.1.1] - 2021-09-01
### Changes
* [Issue #24](https://gitlab.com/axual/public/axual-client-python/-/issues/24) - 
SerDes missing return token

## [1.1.0] - 2021-08-26
### Changes
* [Issue #23](https://gitlab.com/axual/public/axual-client-python/-/issues/23) - 
DeserializingConsumer does not support schema retrieval anymore
* [Issue #22](https://gitlab.com/axual/public/axual-client-python/-/issues/22) - 
Unable to use Consumer/Producer functions without setting Callbacks

## [1.0.0] - 2021-07-20

>>>
**IMPORTANT**
In [this version](https://gitlab.com/axual/public/axual-client-python/-/tree/1.0.0), we have changed the interface to align the client with [Confluent client](https://docs.confluent.io/clients-confluent-kafka-python/current/overview.html). This means you need to update your client applications to use `v1.0.0`. 

Refer [axual-client-python-examples](https://gitlab.com/axual/public/axual-client-python-examples/-/tree/1.0.0) and compare the version [1.0.0-alpha4](https://gitlab.com/axual/public/axual-client-python-examples/-/tree/1.0.0-alpha4) and [1.0.0](https://gitlab.com/axual/public/axual-client-python-examples/-/tree/1.0.0) examples to see how you should modify your Python application.
>>>

### Changes
* [Issue #18](https://gitlab.com/axual/public/axual-client-python/-/issues/18) - 
Align API to allow seamless migration from Confluent client:
    * Removed `AxualClient` class, replaced by `Producer` and `Consumer` classes
    * Removed `ClientConfig` and `SslConfig` classes and introduced dictionary. See [usage](https://gitlab.com/axual/public/axual-client-python/-/tree/master#usage) and for possible configurations refer to the documentation of the underlying [Confluent library](https://docs.confluent.io/platform/current/clients/confluent-kafka-python/html/index.html#kafka-client-configuration)
    * Consumer API: Subscribe to topic should not happen on construct time
    * Dropped legacy `AvroProducer`/`AvroConsumer` dependencies and introduced `serializing_producer`/`deserializing_consumer` classes
* Present client certificate in connection to Discovery API & Schema Registry

### Bug fixes
[Issue #9](https://gitlab.com/axual/public/axual-client-python/-/issues/9) - 
Client fails on comma separated SchemaRegistry url

## [1.0.0-alpha4] - 2021-04-08
### Bug fixes
[Issue #10](https://gitlab.com/axual/public/axual-client-python/-/issues/10) - 
No more than 1 consumer/producer can be instantiated from single AxualClient

## [1.0.0-alpha3] - 2021-04-07
### Bug fixes
[Issue #3](https://gitlab.com/axual/public/axual-client-python/-/issues/3) - 
AxualClient freezes up if discovery can't be completed

[Issue #5](https://gitlab.com/axual/public/axual-client-python/-/issues/5) - 
Discovery thread crashes when discovery result is unavailable

[Issue #6](https://gitlab.com/axual/public/axual-client-python/-/issues/6) - 
Producer delivery report causes error when msg.value() is None

[Issue #8](https://gitlab.com/axual/public/axual-client-python/-/issues/8) - 
v2 suffix required in ClientConfig endpoint

## [1.0.0-alpha2] - 2021-02-18
### Bug fixes
[Issue #4](https://gitlab.com/axual/public/axual-client-python/-/issues/4) - AVROConsumer can never switch

## [1.0.0-alpha] - 2020-10-21
### Added
First alpha release

# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging
import time
import unittest
from time import gmtime

from axualclient.deserializing_consumer import DeserializingConsumer
from axualclient.serializing_producer import SerializingProducer
from tests.integration.Application import Application
from tests.integration.ApplicationLogEvent import ApplicationLogEvent, ApplicationLogLevel
from tests.integration.common import delivery_callback, \
    get_deserializing_consumer_config, get_serializing_producer_config

logger = logging.getLogger(__name__)  # Root logger, to catch all submodule logs
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s.%(msecs)03d|%(levelname)s|%(filename)s| %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
formatter.converter = gmtime  # Log UTC time

TOPIC = 'avro-applicationlog'
MESSAGE_COUNT = 5


class AvroProducerConsumerITTest(unittest.TestCase):
    producer = SerializingProducer(get_serializing_producer_config())
    consumer = DeserializingConsumer(get_deserializing_consumer_config())

    def test_avro_producer_consumer(self):
        logger.info(f'Starting to produce to topic: {TOPIC}.')
        for n in range(MESSAGE_COUNT):
            key = Application(name='value_log_event_producer',
                              version='0.0.1',
                              owner='Axual')
            value = ApplicationLogEvent(timestamp=int(round(time.time() * 1000)),
                                        application=key,
                                        context={'Some key': 'Some Value'},
                                        level=ApplicationLogLevel.INFO,
                                        message=f'Message #{n}')
            self.producer.poll(0)
            self.producer.produce(topic=TOPIC, value=value, key=key, on_delivery=delivery_callback)

        logger.info('Done producing.')
        logger.info('Flushing producer...')
        self.producer.flush()

        # create consumer to consume all produced messages
        count = 0
        start = time.time()

        logger.info(f'Starting consumer to read from topic: {TOPIC}.')
        self.consumer.subscribe([TOPIC])

        while count < MESSAGE_COUNT:
            msg = self.consumer.poll(1.0)
            if msg is not None and not msg.error():
                count += 1
                logger.info(
                    f'Received message on topic {msg.topic()} partition {msg.partition()} '
                    f'offset {msg.offset()} key {msg.key()} value {msg.value()}'
                )
                self.consumer.commit()
            if time.time() - start > 50:
                raise AssertionError('Timed out while waiting for messages to be consumed.')

        logger.info('Committing final offsets.')
        self.consumer.commit()

        logger.info('Leaving group.')
        self.consumer.close()
        self.assertEqual(MESSAGE_COUNT, count)

# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import unittest

from confluent_kafka import TopicPartition

from axualclient.consumer import Consumer
from axualclient.patterns import resolve_topic_partition
from tests.integration.common import get_consumer_config


class ConsumerTest(unittest.TestCase):

    def test_list_topic_with_given_topic_should_not_throw(self):
        consumer = Consumer(configuration=get_consumer_config())
        raised = False
        try:
            consumer.list_topics('topic')
        except TypeError:
            raised = True
        finally:
            consumer.close()
        self.assertFalse(raised, 'TypeError exception raised after calling list_topics with a given topic')

    def test_list_topic_with_empty_topic_should_not_throw(self):
        consumer = Consumer(configuration=get_consumer_config())
        raised = False
        try:
            consumer.list_topics()
        except TypeError:
            raised = True
        finally:
            consumer.close()
        self.assertFalse(raised, 'TypeError exception raised after calling list_topics with a None topic')

    def test_assign_and_assignment_should_work_with_stream_name(self):
        consumer = Consumer(configuration=get_consumer_config())
        topic_partition = TopicPartition(topic='test-topic')
        consumer.assign(partitions=[topic_partition])
        assigned = consumer.assignment()
        assigned_on_kafka = consumer._get_consumer().assignment()
        self.assertEqual('test-topic', assigned[0].topic, 'Expected topic partition test-topic (stream name)')
        self.assertEqual('axual-example-local-test-topic', assigned_on_kafka[0].topic,
                         'Expected full topic name assigned on kafka')
        consumer.close()

    def test_commit_and_committed_should_work_with_stream_name(self):
        consumer = Consumer(configuration=get_consumer_config())

        topic_partition = TopicPartition(topic='string-applicationlog', partition=0, offset=0)

        returned_by_commit = consumer.commit(offsets=[topic_partition], asynchronous=False)
        self.assertEqual('string-applicationlog', returned_by_commit[0].topic,
                         'Expected topic partition test-topic (stream name) returned by commit')

        committed = consumer.committed([topic_partition])
        partition_on_kafka = resolve_topic_partition(consumer.discovery_result, topic_partition)
        committed_on_kafka = consumer._get_consumer().committed([partition_on_kafka])

        self.assertEqual('string-applicationlog', committed[0].topic,
                         'Expected topic partition test-topic (stream name) returned by committed')
        self.assertEqual('axual-example-local-string-applicationlog', committed_on_kafka[0].topic,
                         'Expected full topic name committed on kafka')
        consumer.close()

    def test_commit_returns_none_when_used_async(self):
        consumer = Consumer(configuration=get_consumer_config())

        topic_partition = TopicPartition(topic='string-applicationlog', partition=0, offset=0)

        returned_by_commit = consumer.commit(offsets=[topic_partition], asynchronous=True)
        self.assertEqual(None, returned_by_commit, 'Expected None returned by async commit')

    def test_commit_returns_none_when_used_async_and_no_args(self):
        consumer = Consumer(configuration=get_consumer_config())
        returned_by_commit = consumer.commit()
        self.assertEqual(None, returned_by_commit, 'Expected None returned by async commit')

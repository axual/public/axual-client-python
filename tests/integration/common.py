# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging
import os
from os.path import dirname, abspath
from time import sleep
from time import time
from typing import Optional

import requests
from confluent_kafka.cimpl import KafkaError

from axualclient.avro import AvroSerializer, AvroDeserializer
from tests.integration.Application import Application
from tests.integration.ApplicationLogEvent import ApplicationLogEvent

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def _full_path_of(path):
    base_dir = dirname(dirname(abspath(__file__)))
    return f'{base_dir}{path}'


def delivery_callback(err, msg):
    if err is not None:
        raise AssertionError(f'Failed to deliver message, reason: {err}')
    logger.info(f'Produced record to topic {msg.topic()} partition [{ msg.partition()}] @ offset {msg.offset()}')


def on_error_callback(error: KafkaError):
    raise AssertionError(f'Error consuming messages, reason: {error}')


def on_commit_callback(error: KafkaError, partitions):
    if error is not None:
        raise AssertionError(f'Failed to commit offsets, reason: {error}')
    else:
        logger.info(f'Committed offsets for: {partitions}')


def log_assignment(consumer_obj, partitions):
    logger.info(f'Consumer assignment: {len(partitions)} partitions:')
    for p in partitions:
        logger.info(f'\t{p.topic} [{p.partition}] @ {p.offset}')
    consumer_obj.assign(partitions)


def get_producer_config():
    return {
        # Axual configuration
        'application_id': 'io.axual.example.client.string.producer',
        'endpoint': ENDPOINT,
        'tenant': TENANT,
        'environment': ENVIRONMENT,
        # SSL configuration
        'ssl.certificate.location': SSL_CERTIFICATE_LOCATION,
        'ssl.key.location': SSL_KEY_LOCATION,
        'ssl.ca.location': SSL_CA_LOCATION,
        'ssl.endpoint.identification.algorithm': 'none',
        # Producer configuration
        'acks': 'all',
    }


def get_consumer_config():
    return {
        # Axual configuration
        'application_id': 'io.axual.example.client.string.consumer',
        'endpoint': ENDPOINT,
        'tenant': TENANT,
        'environment': ENVIRONMENT,
        # SSL configuration
        'ssl.certificate.location': SSL_CERTIFICATE_LOCATION,
        'ssl.key.location': SSL_KEY_LOCATION,
        'ssl.ca.location': SSL_CA_LOCATION,
        # Consumer configuration
        'auto.offset.reset': 'earliest',
        'on_commit': on_commit_callback,
        'error_cb': on_error_callback,
        # 'debug': 'all',
        'logger': logger
    }


def get_consumer_config_for_store_offsets():
    return {
        # Axual configuration
        'application_id': 'io.axual.example.client.string.consumer',
        'endpoint': ENDPOINT,
        'tenant': TENANT,
        'environment': ENVIRONMENT,
        # SSL configuration
        'ssl.certificate.location': SSL_CERTIFICATE_LOCATION,
        'ssl.key.location': SSL_KEY_LOCATION,
        'ssl.ca.location': SSL_CA_LOCATION,
        # Consumer configuration
        'auto.offset.reset': 'earliest',
        'error_cb': on_error_callback,
        'logger': logger,
        'enable.auto.offset.store': False,
    }


def get_serializing_producer_config(to_dict=None):
    key_serializer = AvroSerializer(
        schema_str=Application.SCHEMA,
        to_dict=application_to_dict
    )

    value_serializer = AvroSerializer(
        schema_str=ApplicationLogEvent.SCHEMA,
        to_dict=application_log_event_to_dict
    )

    conf = get_producer_config()
    conf['key.serializer'] = key_serializer
    conf['value.serializer'] = value_serializer

    return conf


def get_deserializing_consumer_config():
    key_serializer = AvroDeserializer(
        schema_str=Application.SCHEMA,
        from_dict=dict_to_application
    )

    value_serializer = AvroDeserializer(
        schema_str=ApplicationLogEvent.SCHEMA,
        from_dict=dict_to_application_log_event
    )

    conf = get_consumer_config()
    conf['key.deserializer'] = key_serializer
    conf['value.deserializer'] = value_serializer

    return conf


def application_to_dict(app: Application, ctx) -> dict:
    """
    Returns a dict representation of a Application instance for serialization.

    Args:
        app (Application): Application instance.
        ctx (SerializationContext): Metadata pertaining to the serialization
            operation.
    Returns:
        dict: Dict populated with application attributes to be serialized.
    """
    return dict(
        name=app.name,
        version=app.version,
        owner=app.owner
    )


def application_log_event_to_dict(application_log_event, ctx) -> dict:
    """
    Returns a dict representation of a ApplicationLogEvent instance for serialization.
    Returns:
        dict: Dict populated with ApplicationLogEvent attributes to be serialized.
    """
    return dict(
        timestamp=application_log_event.timestamp,
        source=application_to_dict(application_log_event.source, ctx),
        context=application_log_event.context,
        level=application_log_event.level,
        message=application_log_event.message
    )


def dict_to_application(obj, ctx) -> Optional[Application]:
    """
    Converts object literal(dict) to a Application instance.
    Args:
        obj (dict): Object literal(dict)
        ctx (SerializationContext): Metadata pertaining to the serialization
            operation.
    """
    return None if obj is None else \
        Application(name=obj['name'],
                    version=obj['version'],
                    owner=obj['owner'])


def dict_to_application_log_event(obj, ctx) -> Optional[ApplicationLogEvent]:
    return None if obj is None else \
        ApplicationLogEvent(timestamp=obj['timestamp'],
                            application=dict_to_application(obj['source'], ctx),
                            context=obj['context'],
                            level=obj['level'],
                            message=obj['message'])


def wait_for_standalone():
    """
    Will wait for the Stubs of SchemaRegistryUnit to be present by polling SR and Discovery mocks
    """
    discovery_url = f'{ENDPOINT}v2?applicationId=io.axual.example.client.string.consumer&tenant=axual&env=local'
    sr_url = 'http://localhost:8082/schemas/ids/'
    wait_for_http_response(discovery_url)
    wait_for_http_response(sr_url)


def wait_for_http_response(url):
    sr_stub_down = True
    start = time()
    while sr_stub_down:
        logger.info(f'Waiting for {url} mocks to be present')
        try:
            response = requests.get(url=url, timeout=5)
            if response.ok:
                break
            else:
                sleep(2)
        except Exception:
            sleep(2)
        if time() - start > 30:
            raise AssertionError(f'Did not reach service on {url} within 30 seconds')
    logger.info(f'{url} Responded')


DISCOVERY_API_HOST = 'localhost'
DISCOVERY_API_PORT = os.getenv('STANDALONE_ENDPOINT_PORT', default='8081')
ENDPOINT = f'http://{DISCOVERY_API_HOST}:{DISCOVERY_API_PORT}/'
logger.info(f'Using endpoint: {ENDPOINT}')
TENANT = 'axual'
ENVIRONMENT = 'local'
# SSL configuration
SSL_CERTIFICATE_LOCATION = _full_path_of('/standalone/client-cert/standalone.cer')
SSL_KEY_LOCATION = _full_path_of('/standalone/client-cert/standalone-private.key')
SSL_CA_LOCATION = _full_path_of('/standalone/client-cert/standalone-caroot.cer')
wait_for_standalone()

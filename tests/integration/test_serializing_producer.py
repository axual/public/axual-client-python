# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import unittest

from axualclient.serializing_producer import SerializingProducer
from tests.integration.common import get_serializing_producer_config

serializing_producer = SerializingProducer(configuration=get_serializing_producer_config())


class SerializingProducerTest(unittest.TestCase):

    def test_list_topic_with_given_topic_should_not_throw(self):
        raised = False
        try:
            serializing_producer.list_topics('topic')
        except TypeError:
            raised = True
        self.assertFalse(raised, 'TypeError exception raised after calling list_topics with a given topic')

    def test_list_topic_with_empty_topic_should_not_throw(self):
        raised = False
        try:
            serializing_producer.list_topics()
        except TypeError:
            raised = True
        self.assertFalse(raised, 'TypeError exception raised after calling list_topics with a None topic')

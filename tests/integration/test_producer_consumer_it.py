# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging
import time
import unittest
from time import gmtime

from confluent_kafka import TopicPartition

from axualclient.consumer import Consumer
from axualclient.producer import Producer
from tests.integration.common import get_consumer_config, get_producer_config, delivery_callback, \
    get_consumer_config_for_store_offsets

TIMEOUT = 500

logger = logging.getLogger(__name__)  # Root logger, to catch all submodule logs
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s.%(msecs)03d|%(levelname)s|%(filename)s| %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
formatter.converter = gmtime  # Log UTC time

TOPIC = 'string-applicationlog'
MESSAGE_COUNT = 50


class ProducerConsumerITTest(unittest.TestCase):
    producer = Producer(get_producer_config())
    consumer = Consumer(get_consumer_config())

    def test_string_producer_consumer(self):
        producer = Producer(get_producer_config())
        consumer = Consumer(get_consumer_config())
        logger.info(f'Starting to produce to topic: {TOPIC}.')
        for n in range(MESSAGE_COUNT):
            record_key = f'key_{n}'
            record_value = f'value_{n}'

            producer.poll(0)
            producer.produce(TOPIC, key=record_key, value=record_value, on_delivery=delivery_callback)

        logger.info('Done producing.')
        logger.info('Flushing producer...')
        producer.flush()

        # create consumer to consume all produced messages
        count = 0
        start = time.time()

        logger.info(f'Starting consumer to read from topic: {TOPIC}.')
        consumer.subscribe([TOPIC])
        while count < MESSAGE_COUNT:
            msg = consumer.poll(1.0)
            if msg is not None and not msg.error():
                count += 1
                logger.info(
                    f'Received message on topic {msg.topic()} partition {msg.partition()} '
                    f'offset {msg.offset()} key {msg.key()} value {msg.value()}'
                )
                consumer.commit()
            if time.time() - start > TIMEOUT:
                raise AssertionError('Timed out while waiting for messages to be consumed.')

        logger.info('Committing final offsets and leaving group.')
        consumer.commit()
        consumer.close()
        self.assertEqual(MESSAGE_COUNT, count)

    def test_client_functions(self):
        self.producer = Producer(get_producer_config())
        self.consumer = Consumer(get_consumer_config())
        timestamp = 1234567891234
        for n in range(MESSAGE_COUNT):
            record_key = f'key_{n}'
            record_value = f'value_{n}'

            self.producer.poll(0)
            self.producer.produce(TOPIC, key=record_key, value=record_value, on_delivery=delivery_callback,
                                  timestamp=timestamp + n)

        self.producer.flush()

        self.__kafka_client_should_get_resolved_topics(self.producer._get_producer())
        self.__axual_client_should_get_unresolved_topics(self.producer)

        # create consumer to consume all produced messages
        count = 0
        start = time.time()

        self.consumer.subscribe([TOPIC])

        msg = None

        while count < MESSAGE_COUNT or msg is not None:
            msg = self.consumer.poll(1.0)
            if msg is not None and not msg.error():
                count += 1
                self.consumer.commit()
            if time.time() - start > TIMEOUT:
                raise AssertionError('Timed out while waiting for messages to be consumed.')

        self.__kafka_client_should_get_resolved_topics(self.consumer._get_consumer())
        self.__axual_client_should_get_unresolved_topics(self.consumer)

        self.__test_assignment_should_return_unresolved_topics()

        self.__test_get_watermark_offsets_should_work_with_unresolved_topics()

        self.__test_pause_and_resume_should_work_with_unresolved_topics()

        self.__test_position_should_work_with_unresolved_topics()

        self.__test_seek_and_offsets_for_times_should_work_with_unresolved_topics(timestamp)

        self.consumer.close()
        self.assertLessEqual(MESSAGE_COUNT, count)

    def test_store_offsets(self):
        self.producer = Producer(get_producer_config())
        self.consumer = Consumer(get_consumer_config_for_store_offsets())

        self.consumer.subscribe([TOPIC])

        for n in range(5):
            record_key = f'store_offset_key{n}'
            record_value = f'store_offset_value_{n}'

            self.producer.poll(0)
            self.producer.produce(TOPIC, key=record_key, value=record_value, on_delivery=delivery_callback)

        self.producer.flush()

        msg = self.__test_store_offsets_with_message()

        offsets = self.__test_store_offsets_with_offsets()

        self.__test_store_offsets_with_message_and_offsets_should_throw(msg, offsets)

        self.consumer.close()

    def __kafka_client_should_get_resolved_topics(self, kafka_client):
        list_topics = kafka_client.list_topics()
        topics_keys = list(list_topics.topics.keys())
        self.assertIn('axual-example-local-string-applicationlog', topics_keys,
                      'list_topics() from a kafka client should return resolved topics')
        self.assertIn('axual-example-local-avro-applicationlog', topics_keys,
                      'list_topics() from a kafka client should return resolved topics')
        self.assertEqual('axual-example-local-string-applicationlog',
                         list_topics.topics.get('axual-example-local-string-applicationlog').topic,
                         'list_topics() from a kafka client should return resolved topics in TopicMetadata')

    def __axual_client_should_get_unresolved_topics(self, client):
        list_topics = client.list_topics()
        topics_keys = list(list_topics.topics.keys())
        self.assertIn('string-applicationlog', topics_keys,
                      'list_topics() from an Axual client should return unresolved topics')
        self.assertIn('avro-applicationlog', topics_keys,
                      'list_topics() from an Axual client should return unresolved topics')
        self.assertEqual('string-applicationlog',
                         list_topics.topics.get('string-applicationlog').topic,
                         'list_topics() from an Axual client should return unresolved topics in TopicMetadata')

    def __test_assignment_should_return_unresolved_topics(self):
        for assigned_partition in self.consumer.assignment():
            self.assertEqual('string-applicationlog', assigned_partition.topic,
                             'assignemnt() should return unresolved topics')

    def __test_get_watermark_offsets_should_work_with_unresolved_topics(self):
        assigned_topic_partition = self.consumer.assignment()[0]

        watermarks = self.consumer.get_watermark_offsets(assigned_topic_partition)

        self.assertIsNotNone(watermarks, 'Watermarks should have been retrieved with an unresolved topic')
        self.assertGreater(watermarks[1], 0, 'Expected a value greater than 0 for the high watermark')

    def __test_pause_and_resume_should_work_with_unresolved_topics(self):
        self.consumer.pause(self.consumer.assignment())

        self.producer.produce(TOPIC, key='pause_test_key', value='pause_test_value', on_delivery=delivery_callback)
        self.producer.flush()

        msg = self.consumer.poll(2.0)
        self.assertIsNone(msg, 'Pause should work with unresolved topics')

        self.consumer.resume(self.consumer.assignment())

        msg = self.consumer.poll(2.0)
        self.assertIsNotNone(msg, 'Resume should work with unresolved topics')

    def __test_position_should_work_with_unresolved_topics(self):
        partitions = self.consumer.position(self.consumer.assignment())
        self.assertIsNotNone(partitions)
        self.assertEqual('string-applicationlog', partitions[0].topic,
                         'position() should return unresolved topics')

    def __test_seek_and_offsets_for_times_should_work_with_unresolved_topics(self, timestamp):
        msg = self.consumer.poll(1.0)
        self.assertIsNone(msg)
        partitions_at_time = self.consumer.offsets_for_times([TopicPartition(TOPIC, 0, timestamp + 1),
                                                              TopicPartition(TOPIC, 1, timestamp + 1)])

        partition0 = TopicPartition(TOPIC, partitions_at_time[0].partition, partitions_at_time[0].offset)
        partition1 = TopicPartition(TOPIC, partitions_at_time[1].partition, partitions_at_time[1].offset)

        self.consumer.seek(partition0)
        self.consumer.seek(partition1)

        msg = self.consumer.poll(1.0)

        self.assertIsNotNone(msg)

        msg_offset = msg.offset()
        msg_partition = msg.partition()

        self.assertTrue(msg_offset == partitions_at_time[0].offset and msg_partition == partitions_at_time[0].partition
                        or
                        msg_offset == partitions_at_time[1].offset and msg_partition == partitions_at_time[1].partition)

    def __test_store_offsets_with_message(self):
        msg = None
        while msg is None:
            msg = self.consumer.poll(1.0)
        self.consumer.store_offsets(message=msg)
        return msg

    def __test_store_offsets_with_offsets(self):
        assignments = self.consumer.assignment()
        self.consumer.store_offsets(offsets=assignments)
        return assignments

    def __test_store_offsets_with_message_and_offsets_should_throw(self, msg, assignments):
        with self.assertRaises(ValueError) as value_error_exception:
            self.consumer.store_offsets(message=msg, offsets=assignments)
        self.assertEqual('message and offsets are mutually exclusive', str(value_error_exception.exception))

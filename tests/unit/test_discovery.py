# -*- coding: utf-8 -*-
#
#      Copyright (C) 2021 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import json
import unittest
from unittest import mock
from time import sleep

from requests import RequestException

from axualclient.discovery import DiscoveryFetcher, DiscoveryClient, BOOTSTRAP_SERVERS_KEY, TTL_KEY, V2_ENDPOINT_SUFFIX
from axualclient.exception import NoClusterAvailableException

# Fixtures
ENDPOINT_200 = 'https://discovery-api-200.com/'
ENDPOINT_204 = 'https://discovery-api-204.com/'
ENDPOINT_403 = 'https://discovery-api-403.com/'
ENDPOINT_503 = 'https://discovery-api-503.com/'

DISCOVERY_RESULT_200 = {
    BOOTSTRAP_SERVERS_KEY: 'bootstrap.servers.url',
    TTL_KEY: '600000'
}

MOCK_CONFIG = {
    'endpoint': '',
    'tenant': 'tenant',
    'environment': 'env',
    'application_id': 'app.id',
    'ssl.ca.location': 'ca_location',
    'ssl.key.location': 'key_location',
    'ssl.certificate.location': 'cert_location',
}


def mocked_requests_get(*args, **kwargs):
    """ This method mocks requests.get """
    class MockResponse:
        def __init__(self, content, status_code):
            self.content = json.dumps(content)
            self.status_code = status_code
            self.ok = status_code == 200

    if args[0] == ENDPOINT_200:
        return MockResponse(DISCOVERY_RESULT_200, 200)
    if args[0] == ENDPOINT_204:
        return MockResponse({}, 204)
    elif args[0] == ENDPOINT_403:
        return MockResponse({}, 403)
    if args[0] == ENDPOINT_503:
        return MockResponse({}, 503)


class TestDiscoveryClient(DiscoveryClient):
    """ Only holds the discovery_result for test purposes. """
    def __init__(self):
        self.discovery_result = None

    def on_discovery_properties_changed(self, discovery_result: dict):
        self.discovery_result = discovery_result


class DiscoveryFetcherTest(unittest.TestCase):

    @mock.patch('axualclient.discovery.requests.get', side_effect=[mocked_requests_get(ENDPOINT_200)])
    def test_discovery_should_append_v2(self, mock_get):
        test_client = TestDiscoveryClient()
        MOCK_CONFIG['endpoint'] = ENDPOINT_200

        fetcher = DiscoveryFetcher(MOCK_CONFIG, test_client)
        fetcher.wait_for_discovery_result()
        fetcher.deregister_discovery_client(test_client)

        self.assertEqual(ENDPOINT_200 + V2_ENDPOINT_SUFFIX, fetcher.endpoint)

    @mock.patch('axualclient.discovery.requests.get', side_effect=[mocked_requests_get(ENDPOINT_200)])
    def test_continuous_discovery_should_query_after_the_sleeping_time(self, mock_get):
        DiscoveryFetcher.set_backoff_period(1)
        test_client = TestDiscoveryClient()
        MOCK_CONFIG['endpoint'] = ENDPOINT_200

        fetcher = DiscoveryFetcher(MOCK_CONFIG, test_client)

        # The first time that the continuous_discovery cycle will start, it will find self.discovery_result = {}
        # And then will go to sleep for BACKOFF_PERIOD = 1
        sleep(1.5)

        fetcher.deregister_discovery_client(test_client)

        self.assertLessEqual(2, mock_get.call_count,
                            msg='Should call the discovery again after the backoff period')

    @mock.patch('axualclient.discovery.requests.get', side_effect=[mocked_requests_get(ENDPOINT_200)])
    def test_discovery_works_after_number_of_clients_raised_from_zero(self, mock_get):
        test_client = TestDiscoveryClient()
        MOCK_CONFIG['endpoint'] = ENDPOINT_200

        fetcher = DiscoveryFetcher(MOCK_CONFIG, test_client)
        fetcher.wait_for_discovery_result()
        fetcher.deregister_discovery_client(test_client)

        test_client = TestDiscoveryClient()

        fetcher.register_discovery_client(test_client)
        fetcher.wait_for_discovery_result()
        fetcher.deregister_discovery_client(test_client)

        self.assertIsNotNone(fetcher.discovery_result,
                            msg='Should call the discovery again when number of clients raise from 0 to 1')

    @mock.patch('axualclient.discovery.requests.get',
                side_effect=[mocked_requests_get(ENDPOINT_200)])
    def test_discovery_result_should_not_get_updated_when_there_are_no_clients(self, mock_get):
        DiscoveryFetcher.set_backoff_period(1)
        test_client = TestDiscoveryClient()
        MOCK_CONFIG['endpoint'] = ENDPOINT_200

        fetcher = DiscoveryFetcher(MOCK_CONFIG, test_client)
        fetcher.wait_for_discovery_result()
        fetcher.deregister_discovery_client(test_client)

        fetcher.discovery_result = None

        sleep(1.5)

        self.assertIsNone(fetcher.discovery_result,
                             msg='Should not execute discovery query if there are no clients')

    @mock.patch('axualclient.discovery.requests.get', side_effect=[mocked_requests_get(ENDPOINT_200)])
    def test_discovery_200(self, mock_get):
        test_client = TestDiscoveryClient()
        MOCK_CONFIG['endpoint'] = ENDPOINT_200

        fetcher = DiscoveryFetcher(MOCK_CONFIG, test_client)
        fetcher.wait_for_discovery_result()
        fetcher.deregister_discovery_client(test_client)

        self.assertIsNotNone(test_client.discovery_result,
                             msg='Valid discovery result returned')
        self.assertEqual(1, mock_get.call_count,
                         msg='Should call GET at least once')
        self.assertEqual(DISCOVERY_RESULT_200[BOOTSTRAP_SERVERS_KEY], fetcher.discovery_result[BOOTSTRAP_SERVERS_KEY],
                         msg='Expected bootstrap servers')

    @mock.patch('axualclient.discovery.requests.get', side_effect=[mocked_requests_get(ENDPOINT_204)])
    def test_discovery_204_on_first_call(self, mock_get):
        test_client = TestDiscoveryClient()
        MOCK_CONFIG['endpoint'] = ENDPOINT_204

        fetcher = DiscoveryFetcher(MOCK_CONFIG, test_client)
        fetcher.deregister_discovery_client(test_client)

        self.assertEqual(ENDPOINT_204 + V2_ENDPOINT_SUFFIX, fetcher.endpoint)
        self.assertRaises(NoClusterAvailableException, fetcher.wait_for_discovery_result)
        self.assertIsNone(test_client.discovery_result,
                          msg='None discovery result returned.')
        self.assertEqual(1, mock_get.call_count,
                         msg='Should call GET at least once')
        self.assertTrue(fetcher.stop_discovery,
                        msg='Should stop after initial failure')

    @mock.patch('axualclient.discovery.requests.get', side_effect=[mocked_requests_get(ENDPOINT_403)])
    def test_discovery_403_on_first_call(self, mock_get):
        test_client = TestDiscoveryClient()
        MOCK_CONFIG['endpoint'] = ENDPOINT_403

        fetcher = DiscoveryFetcher(MOCK_CONFIG, test_client)
        fetcher.deregister_discovery_client(test_client)

        self.assertEqual(ENDPOINT_403 + V2_ENDPOINT_SUFFIX, fetcher.endpoint)
        self.assertRaises(NoClusterAvailableException, fetcher.wait_for_discovery_result)
        self.assertIsNone(test_client.discovery_result,
                          msg='None discovery result returned.')
        self.assertEqual(1, mock_get.call_count,
                         msg='Should call GET at least once')
        self.assertTrue(fetcher.stop_discovery,
                        msg='Should stop after initial failure')

    @mock.patch('axualclient.discovery.requests.get', side_effect=[mocked_requests_get(ENDPOINT_503)])
    def test_discovery_503_on_first_call(self, mock_get):
        test_client = TestDiscoveryClient()
        MOCK_CONFIG['endpoint'] = ENDPOINT_503

        fetcher = DiscoveryFetcher(MOCK_CONFIG, test_client)
        fetcher.deregister_discovery_client(test_client)

        self.assertEqual(ENDPOINT_503 + V2_ENDPOINT_SUFFIX, fetcher.endpoint)
        self.assertRaises(RequestException, fetcher.wait_for_discovery_result)
        self.assertIsNone(test_client.discovery_result,
                          msg='None discovery result returned.')
        self.assertEqual(1, mock_get.call_count,
                         msg='Should call GET at least once')
        self.assertTrue(fetcher.stop_discovery,
                        msg='Should stop after initial failure')

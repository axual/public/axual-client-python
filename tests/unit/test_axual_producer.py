# -*- coding: utf-8 -*-
#
#      Copyright (C) 2021 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
from unittest import mock
from unittest.mock import patch

from confluent_kafka import TopicPartition
from confluent_kafka.admin import GroupMetadata, ClusterMetadata, TopicMetadata, PartitionMetadata

import axualclient.axual_producer

LIST_TOPIC_PARTITIONS = [TopicPartition('TP1', 0, 0), TopicPartition('TP2', 1, 1)]


@mock.patch('axualclient.axual_producer._AxualProducer.__abstractmethods__', set())
@mock.patch('axualclient.discovery.DiscoveryClientRegistry.register_client')
@mock.patch('confluent_kafka.cimpl.Producer')
class AxualProducerTest(unittest.TestCase):

    def test_abort_transaction_should_work_with_no_timeout(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.abort_transaction()
            mock_kafka_producer.abort_transaction.assert_called_with()

    def test_abort_transaction_should_work_with_timeout(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.abort_transaction(1)
            mock_kafka_producer.abort_transaction.assert_called_with(1)

    def test_begin_transaction(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.begin_transaction()
            mock_kafka_producer.begin_transaction.assert_called_with()

    def test_commit_transaction_should_work_with_no_timeout(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.commit_transaction()
            mock_kafka_producer.commit_transaction.assert_called_with()

    def test_commit_transaction_should_work_with_timeout(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.commit_transaction(1)
            mock_kafka_producer.commit_transaction.assert_called_with(1)

    def test_flush_should_work_with_no_timeout(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.flush()
            mock_kafka_producer.flush.assert_called_with()

    def test_flush_should_work_with_timeout(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.flush(1)
            mock_kafka_producer.flush.assert_called_with(1)

    def test_init_transactions(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.init_transactions(1)
            mock_kafka_producer.init_transactions.assert_called_with(1)

    def test_poll_should_work_with_no_timeout(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.poll()
            mock_kafka_producer.poll.assert_called_with()

    def test_poll_should_work_with_timeout_in_kwargs(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.poll(1)
            mock_kafka_producer.poll.assert_called_with(timeout=1)

    def test_purge_should_use_defaults(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.purge()
            mock_kafka_producer.purge.assert_called_with(True, True, True)

    def test_purge_should_not_use_defaults_when_values_are_provided(self, mock, mock_kafka_producer):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.purge(False, False, False)
            mock_kafka_producer.purge.assert_called_with(False, False, False)

    @mock.patch('axualclient.axual_producer.resolve_topic', return_value='resolved_topic')
    def test_produce_with_no_arguments_should_use_defaults(self, mock, mock_kafka_producer, resolve_topic_mock):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.produce('topic')
            mock_kafka_producer.produce.assert_called_with('resolved_topic')

    @mock.patch('axualclient.axual_producer.resolve_topic', return_value='resolved_topic')
    def test_produce_with_arguments_should_use_params(self, mock, mock_kafka_producer, resolve_topic_mock):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            axual_producer.produce('topic', 'value_', 'key_', 1, axualclient.axual_producer._AxualProducer, 123, {})
            mock_kafka_producer.produce.assert_called_with('resolved_topic', value='value_', key='key_', partition=1,
                                                           on_delivery=axualclient.axual_producer._AxualProducer,
                                                           timestamp=123, headers={})

    @mock.patch('axualclient.axual_producer.resolve_topic_partitions', return_value=LIST_TOPIC_PARTITIONS)
    def test_send_offsets_to_transaction_without_timeout(
            self, mock, mock_kafka_producer, resolve_topic_partitions_mock):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            group_metadata = GroupMetadata()
            axual_producer.send_offsets_to_transaction([], group_metadata)
            mock_kafka_producer.send_offsets_to_transaction.assert_called_with(LIST_TOPIC_PARTITIONS, group_metadata)

    @mock.patch('axualclient.axual_producer.resolve_topic_partitions', return_value=LIST_TOPIC_PARTITIONS)
    def test_send_offsets_to_transaction_with_timeout(self, mock, mock_kafka_producer, resolve_topic_partitions_mock):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            group_metadata = GroupMetadata()
            axual_producer.send_offsets_to_transaction([], group_metadata, 1)
            mock_kafka_producer.send_offsets_to_transaction.assert_called_with(
                LIST_TOPIC_PARTITIONS, group_metadata, timeout=1)

    @mock.patch('axualclient.axual_producer.resolve_topic', return_value='resolved_topic')
    @mock.patch('axualclient.axual_producer.unresolve_topics_in_cluster_metadata',
                return_value={'resolved_topic': TopicMetadata()})
    def test_list_topics(self, mock, mock_kafka_producer, resolve_topic_mock, unresolved_topic_mock):
        axual_producer = axualclient.axual_producer._AxualProducer({})
        cluster_metadata = ClusterMetadata()
        topic_metadata = TopicMetadata()
        topic_metadata.topic = 'unresolved_topic'
        partition_metadata = PartitionMetadata()
        topic_metadata.partitions = {0: partition_metadata}
        cluster_metadata.topics = {'unresolved_topic': topic_metadata}
        with patch.object(axual_producer, '_get_producer', return_value=mock_kafka_producer):
            with patch.object(mock_kafka_producer, 'list_topics'):
                result = axual_producer.list_topics()
                self.assertIn('resolved_topic', result.topics.keys())
                mock_kafka_producer.list_topics.assert_called_with('resolved_topic', -1)

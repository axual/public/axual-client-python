# -*- coding: utf-8 -*-
#
#      Copyright (C) 2021 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
from unittest import mock
from unittest.mock import patch

from confluent_kafka import TopicPartition
from confluent_kafka.admin import ClusterMetadata, TopicMetadata, PartitionMetadata

import axualclient.axual_consumer
import axualclient.axual_producer
from axualclient.axual_producer import _AxualProducer

UNRESOLVED_PARTITION = TopicPartition('topic', 0, 0)

RESOLVED_PARTITION = TopicPartition('resolved', 0, 0)


@mock.patch('axualclient.axual_consumer._AxualConsumer.__abstractmethods__', set())
@mock.patch('axualclient.discovery.DiscoveryClientRegistry.register_client')
@mock.patch('confluent_kafka.cimpl.Consumer')
class AxualConsumerTest(unittest.TestCase):

    @mock.patch('axualclient.axual_consumer.resolve_topic_partitions', return_value=TopicPartition('resolved', 0, 0))
    def test_assign_should_send_resolved_partitions(self, register_client_mock, mocked_kafka_consumer, resolve_mock):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.assign([UNRESOLVED_PARTITION])
            mocked_kafka_consumer.assign.assert_called_with(RESOLVED_PARTITION)

    def test_assign_empty_unassign(self, register_client_mock, mocked_kafka_consumer):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.assign([])
            mocked_kafka_consumer.assign.assert_called_with([])

    @mock.patch('axualclient.axual_consumer.unresolve_topic_partitions', return_value=TopicPartition('topic', 0, 0))
    def test_assignment_should_return_unresolved_partitions(self, register_mock, mocked_kafka_consumer, unresolve_mock):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            with patch.object(mocked_kafka_consumer, 'assignment', return_value=RESOLVED_PARTITION):
                returned = axual_consumer.assignment()
                self.assertEqual(UNRESOLVED_PARTITION, returned)

    def test_commit_should_work_with_no_args(self, register_client_mock, mocked_kafka_consumer):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.commit()
            mocked_kafka_consumer.commit.assert_called_with(asynchronous=True)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partitions', return_value=RESOLVED_PARTITION)
    @mock.patch('axualclient.axual_consumer.unresolve_topic_partitions', return_value=UNRESOLVED_PARTITION)
    def test_commit_should_correctly_pass_resolved_and_should_return_unresolved_partitions(self, register_client_mock, mocked_kafka_consumer, resolve, unresolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            returned = axual_consumer.commit(offsets=[UNRESOLVED_PARTITION], asynchronous=False)
            mocked_kafka_consumer.commit.assert_called_with(offsets=RESOLVED_PARTITION, asynchronous=False)
            self.assertEqual(UNRESOLVED_PARTITION, returned)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partitions', return_value=RESOLVED_PARTITION)
    @mock.patch('axualclient.axual_consumer.unresolve_topic_partitions', return_value=UNRESOLVED_PARTITION)
    def test_committed_should_pass_resolved_and_should_return_unresolved_partitions(
            self, register, mocked_kafka_consumer, resolve, unresolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            returned = axual_consumer.committed([UNRESOLVED_PARTITION])
            mocked_kafka_consumer.committed.assert_called_with(RESOLVED_PARTITION)
            self.assertEqual(UNRESOLVED_PARTITION, returned)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partitions', return_value=RESOLVED_PARTITION)
    @mock.patch('axualclient.axual_consumer.unresolve_topic_partitions', return_value=UNRESOLVED_PARTITION)
    def test_committed_should_pass_resolved_and_should_return_unresolved_partitions_with_timeout(
            self, register, mocked_kafka_consumer, resolve, unresolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            returned = axual_consumer.committed([UNRESOLVED_PARTITION], 1.0)
            mocked_kafka_consumer.committed.assert_called_with(RESOLVED_PARTITION, 1.0)
            self.assertEqual(UNRESOLVED_PARTITION, returned)

    def test_consume_should_work_without_params(self, register, mocked_kafka_consumer):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.consume()
            mocked_kafka_consumer.consume.assert_called_with(1, -1.0)

    def test_consume_should_work_with_params(self, register, mocked_kafka_consumer):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.consume(2, 3)
            mocked_kafka_consumer.consume.assert_called_with(2, 3.0)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partition', return_value=RESOLVED_PARTITION)
    def test_get_watermark_offsets_should_use_resolved_partition(self, register, mocked_kafka_consumer, resolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.get_watermark_offsets(UNRESOLVED_PARTITION)
            mocked_kafka_consumer.get_watermark_offsets.assert_called_with(partition=RESOLVED_PARTITION, cached=False)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partition', return_value=RESOLVED_PARTITION)
    def test_get_watermark_offsets_should_use_resolved_partition_with_params(
            self, register, mocked_kafka_consumer, resolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.get_watermark_offsets(UNRESOLVED_PARTITION, 1.0, True)
            mocked_kafka_consumer.get_watermark_offsets.assert_called_with(
                partition=RESOLVED_PARTITION, timeout=1.0, cached=True)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partitions', return_value=RESOLVED_PARTITION)
    @mock.patch('axualclient.axual_consumer.unresolve_topic_partitions', return_value=UNRESOLVED_PARTITION)
    def test_offsets_for_times_should_pass_resolved_partitions_and_return_unresolved_partitions(
            self, register, mocked_kafka_consumer, resolve, unresolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            result = axual_consumer.offsets_for_times([UNRESOLVED_PARTITION])
            mocked_kafka_consumer.offsets_for_times.assert_called_with(RESOLVED_PARTITION)
            self.assertEqual(UNRESOLVED_PARTITION, result)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partitions', return_value=RESOLVED_PARTITION)
    @mock.patch('axualclient.axual_consumer.unresolve_topic_partitions', return_value=UNRESOLVED_PARTITION)
    def test_offsets_for_times_should_pass_resolved_partitions_and_return_unresolved_partitions_with_timeout(
            self, register, mocked_kafka_consumer, resolve, unresolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            result = axual_consumer.offsets_for_times([UNRESOLVED_PARTITION], 8.0)
            mocked_kafka_consumer.offsets_for_times.assert_called_with(RESOLVED_PARTITION, 8.0)
            self.assertEqual(UNRESOLVED_PARTITION, result)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partitions', return_value=RESOLVED_PARTITION)
    def test_pause_should_use_resolved_partitions(self, register, mocked_kafka_consumer, resolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.pause([UNRESOLVED_PARTITION])
            mocked_kafka_consumer.pause.assert_called_with(RESOLVED_PARTITION)

    def test_poll_should_work_without_params(self, register, mocked_kafka_consumer):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.poll()
            mocked_kafka_consumer.poll.assert_called_with()

    def test_poll_should_work_with_params(self, register, mocked_kafka_consumer):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.poll(1)
            mocked_kafka_consumer.poll.assert_called_with(1.0)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partitions', return_value=RESOLVED_PARTITION)
    @mock.patch('axualclient.axual_consumer.unresolve_topic_partitions', return_value=UNRESOLVED_PARTITION)
    def test_position_should_pass_resolved_partitions_and_return_unresolved_partitions(
            self, register, mocked_kafka_consumer, resolve, unresolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            result = axual_consumer.position([UNRESOLVED_PARTITION])
            mocked_kafka_consumer.position.assert_called_with(RESOLVED_PARTITION)
            self.assertEqual(UNRESOLVED_PARTITION, result)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partitions', return_value=RESOLVED_PARTITION)
    def test_resume_should_use_resolved_partitions(self, register, mocked_kafka_consumer, resolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.resume([UNRESOLVED_PARTITION])
            mocked_kafka_consumer.resume.assert_called_with(RESOLVED_PARTITION)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partition', return_value=RESOLVED_PARTITION)
    def test_seek_should_use_resolved_partitions(self, register, mocked_kafka_consumer, resolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.seek(UNRESOLVED_PARTITION)
            mocked_kafka_consumer.seek.assert_called_with(RESOLVED_PARTITION)

    @mock.patch('axualclient.axual_consumer.resolve_topic_partitions', return_value=RESOLVED_PARTITION)
    def test_store_offsets_should_use_resolved_partitions_if_given(
            self, register, mocked_kafka_consumer, resolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.store_offsets(offsets=[UNRESOLVED_PARTITION])
            mocked_kafka_consumer.store_offsets.assert_called_with(offsets=RESOLVED_PARTITION)

    @mock.patch('axualclient.axual_consumer.resolve_topics', return_value='axual-ins-env-general-test')
    def test_subscribe_should_use_resolved_topics(self, register, mocked_kafka_consumer, resolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.subscribe(['general-test'])
            mocked_kafka_consumer.subscribe.assert_called_with('axual-ins-env-general-test')

    @mock.patch('axualclient.axual_consumer.resolve_topics', return_value='axual-ins-env-general-test')
    def test_subscribe_should_use_resolved_topics_with_params(self, register, mocked_kafka_consumer, resolve):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        with patch.object(axual_consumer, '_get_consumer', return_value=mocked_kafka_consumer):
            axual_consumer.subscribe(['general-test'], _AxualProducer,
                                     axualclient.axual_consumer._AxualConsumer, AxualConsumerTest)
            mocked_kafka_consumer.subscribe.assert_called_with('axual-ins-env-general-test', on_assign=_AxualProducer,
                                                               on_revoke=axualclient.axual_consumer._AxualConsumer,
                                                               on_lost=AxualConsumerTest)

    @mock.patch('axualclient.axual_consumer.resolve_topic', return_value='resolved_topic')
    @mock.patch('axualclient.axual_consumer.unresolve_topics_in_cluster_metadata',
                return_value={'resolved_topic': TopicMetadata()})
    def test_list_topics(self, mock, mock_kafka_consumer, resolve_topic_mock, unresolved_topic_mock):
        axual_consumer = axualclient.axual_consumer._AxualConsumer({})
        cluster_metadata = ClusterMetadata()
        topic_metadata = TopicMetadata()
        topic_metadata.topic = 'unresolved_topic'
        partition_metadata = PartitionMetadata()
        topic_metadata.partitions = {0: partition_metadata}
        cluster_metadata.topics = {'unresolved_topic': topic_metadata}
        with patch.object(axual_consumer, '_get_consumer', return_value=mock_kafka_consumer):
            with patch.object(mock_kafka_consumer, 'list_topics'):
                result = axual_consumer.list_topics()
                self.assertIn('resolved_topic', result.topics.keys())
                mock_kafka_consumer.list_topics.assert_called_with(topic='resolved_topic', timeout=-1)

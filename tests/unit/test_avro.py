#
#      Copyright (C) 2021 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from unittest import TestCase, mock

from axualclient.avro import AvroSerializer, AvroDeserializer


class Test(TestCase):

    def test_avro_serializer_call_unconfigured(self):
        with self.assertRaises(RuntimeError):
            AvroSerializer(schema_str='')(None, None)

    @mock.patch('axualclient.avro.SchemaRegistryClient')
    @mock.patch('axualclient.avro.KafkaAvroSerializer')
    def test_avro_serializer_configure(self, mock_schema_registry_client, mock_avro_serializer):
        config = {}
        serializer = AvroSerializer(schema_str='')

        serializer.configure(config)

        mock_schema_registry_client.assert_called()
        mock_avro_serializer.assert_called()

    ################################################################################################

    def test_avro_deserializer_call_unconfigured(self):
        with self.assertRaises(RuntimeError):
            AvroDeserializer()(None, None)

    @mock.patch('axualclient.avro.SchemaRegistryClient')
    @mock.patch('axualclient.avro.KafkaAvroDeserializer')
    def test_avro_deserializer_configure(self, mock_schema_registry_client, mock_avro_deserializer):
        config = {}
        serializer = AvroDeserializer()

        serializer.configure(config)

        mock_schema_registry_client.assert_called()
        mock_avro_deserializer.assert_called()

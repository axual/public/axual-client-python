#
#      Copyright (C) 2021 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from unittest import TestCase

from confluent_kafka import TopicPartition
from confluent_kafka.admin import ClusterMetadata, TopicMetadata, PartitionMetadata

from axualclient.patterns import resolve_topic, resolve_group, unresolve_topic, \
    TENANT_KEY, ENVIRONMENT_KEY, INSTANCE_KEY, TOPIC_PATTERN_KEY, GROUP_PATTERN_KEY, resolve_topics, \
    resolve_topic_partition, resolve_topic_partitions, unresolve_topic_partition, \
    unresolve_topic_partitions, unresolve_topics_in_cluster_metadata

TOPIC_PATTERN = '{' + TENANT_KEY + '}-{' + INSTANCE_KEY + '}-{' + ENVIRONMENT_KEY + '}-{topic}'
GROUP_PATTERN = '{' + TENANT_KEY + '}.{' + ENVIRONMENT_KEY + '}.{group}'

ENVIRONMENT = 'environment'
TENANT = 'axual'
INSTANCE = 'ins'

DISCOVERY_RESULT = {
    TENANT_KEY: TENANT,
    INSTANCE_KEY: INSTANCE,
    ENVIRONMENT_KEY: ENVIRONMENT,
    TOPIC_PATTERN_KEY: TOPIC_PATTERN,
    GROUP_PATTERN_KEY: GROUP_PATTERN,
}


class Test(TestCase):

    def test_resolve_topic(self):
        self.assertEqual(TENANT + '-' + INSTANCE + '-' + ENVIRONMENT + '-general-test',
                         resolve_topic(DISCOVERY_RESULT, 'general-test'))

    def test_resolve_topic_none(self):
        self.assertEqual(None, resolve_topic(DISCOVERY_RESULT, None))

    def test_resolve_topics(self):
        topics = ['general-test', 'sergeant-test']
        self.assertListEqual([TENANT + '-' + INSTANCE + '-' + ENVIRONMENT + '-general-test',
                              TENANT + '-' + INSTANCE + '-' + ENVIRONMENT + '-sergeant-test'],
                             resolve_topics(DISCOVERY_RESULT, topics))

    def test_resolve_topics_with_none(self):
        topics = ['general-test', None]
        self.assertListEqual([TENANT + '-' + INSTANCE + '-' + ENVIRONMENT + '-general-test', None],
                             resolve_topics(DISCOVERY_RESULT, topics))

    def test_resolve_group(self):
        self.assertEqual(TENANT + '.' + ENVIRONMENT + '.general.test',
                         resolve_group(DISCOVERY_RESULT, 'general.test'))

    def test_resolve_group_none(self):
        self.assertEqual(None, resolve_group(DISCOVERY_RESULT, None))

    def test_unresolve_topic(self):
        topic = 'axual-ins-environment-general-test'
        self.assertEqual('general-test', unresolve_topic(DISCOVERY_RESULT, topic))

    def test_resolve_topic_partition(self):
        partition = TopicPartition('general-test', 0, 5)
        resolved_partition = resolve_topic_partition(DISCOVERY_RESULT, partition)
        self.assertEqual('axual-ins-environment-general-test', resolved_partition.topic)
        self.assertEqual(0, resolved_partition.partition)
        self.assertEqual(5, resolved_partition.offset)

    def test_resolve_topic_partitions(self):
        partition_one = TopicPartition('general-test', 0, 5)
        partition_two = TopicPartition('sergeant-test', 1, 8)
        partitions = [partition_one, partition_two]
        resolved_partitions = resolve_topic_partitions(DISCOVERY_RESULT, partitions)
        self.assertEqual('axual-ins-environment-general-test', resolved_partitions[0].topic)
        self.assertEqual(0, resolved_partitions[0].partition)
        self.assertEqual(5, resolved_partitions[0].offset)
        self.assertEqual('axual-ins-environment-sergeant-test', resolved_partitions[1].topic)
        self.assertEqual(1, resolved_partitions[1].partition)
        self.assertEqual(8, resolved_partitions[1].offset)

    def test_resolve_topic_partitions_with_no_partitions_should_return_None(self):
        self.assertIsNone(resolve_topic_partitions(DISCOVERY_RESULT, None))

    def test_resolve_topic_partitions_with_empty_partitions_should_return_empty(self):
        self.assertEquals([], resolve_topic_partitions(DISCOVERY_RESULT, []))

    def test_unresolve_topic_partition(self):
        unresolved_partition = unresolve_topic_partition(
            DISCOVERY_RESULT, TopicPartition('axual-ins-environment-general-test', 0, 1))
        self.assertEquals('general-test', unresolved_partition.topic)

    def test_unresolve_topic_partitions(self):
        partitions = [TopicPartition('axual-ins-environment-general-test', 0, 1),
                      TopicPartition('axual-ins-environment-sergeant-test', 0, 1)]
        unresolved_partitions = unresolve_topic_partitions(DISCOVERY_RESULT, partitions)
        self.assertEquals('general-test', unresolved_partitions[0].topic)
        self.assertEquals('sergeant-test', unresolved_partitions[1].topic)

    def test_unresolve_topic_partition_with_no_partitions_should_return_None(self):
        self.assertIsNone(unresolve_topic_partition(DISCOVERY_RESULT, None))

    def test_unresolve_topic_partitions_with_empty_partitions_should_return_None(self):
        self.assertIsNone(unresolve_topic_partitions(DISCOVERY_RESULT, []))

    def test_unresolve_topic_should_return_None_with_None_string(self):
        result = unresolve_topic(DISCOVERY_RESULT, None)
        self.assertEqual(None, result)

    def test_unresolve_topic_should_return_the_original_string_with_not_well_formatted_topic(self):
        result = unresolve_topic(DISCOVERY_RESULT, 'wrong')
        self.assertEqual('wrong', result)

    def test_unresolve_topic_should_return_the_original_string_when_topic_is_missing_tenant_key(self):
        result = unresolve_topic(DISCOVERY_RESULT, 'ins-environment-general-test')
        self.assertEqual('ins-environment-general-test', result)

    def test_unresolve_topic_should_return_the_original_string_when_topic_is_missing_instance_key(self):
        result = unresolve_topic(DISCOVERY_RESULT, 'axual-environment-general-test')
        self.assertEqual('axual-environment-general-test', result)

    def test_unresolve_topic_should_return_the_original_string_when_topic_is_missing_environment_key(self):
        result = unresolve_topic(DISCOVERY_RESULT, 'axual-ins-general-test')
        self.assertEqual('axual-ins-general-test', result)

    def test_unresolve_topic_should_return_the_original_string_when_topic_is_missing_stream_name(self):
        result = unresolve_topic(DISCOVERY_RESULT, 'axual-ins-environment')
        self.assertEqual('axual-ins-environment', result)

    def test_unresolve_topic_should_return_the_original_string_when_topic_is_missing_stream_name_with_dash(self):
        result = unresolve_topic(DISCOVERY_RESULT, 'axual-ins-environment-')
        self.assertEqual('axual-ins-environment-', result)

    def test_unresolve_topic_with_different_pattern(self):
        DISCOVERY_RESULT['topic.pattern'] = '{' + TENANT_KEY + '}-{' + ENVIRONMENT_KEY + '}-{topic}'
        result = unresolve_topic(DISCOVERY_RESULT, 'axual-environment-general-test')
        self.assertEqual('general-test', result)

    def test_unresolve_topics_in_cluster_metadata(self):
        cluster_metadata = ClusterMetadata()
        topic_metadata = TopicMetadata()

        topic_metadata.topic = 'axual-ins-environment-general-test'
        topic_metadata.partitions = {0: PartitionMetadata()}

        topic_metadata__consumer_offsets = TopicMetadata()

        topic_metadata__consumer_offsets.topic = '__consumer_offsets'
        topic_metadata__consumer_offsets.partitions = {0: PartitionMetadata()}

        # <key: topic_name, value: TopicMetadata>
        topics = {'axual-ins-environment-general-test': topic_metadata,
                  '__consumer_offsets': topic_metadata__consumer_offsets}
        cluster_metadata.topics = topics

        unresolved_topics = unresolve_topics_in_cluster_metadata(DISCOVERY_RESULT, cluster_metadata)

        self.assertIn('general-test', unresolved_topics.keys())
        self.assertEqual(unresolved_topics.get('general-test').topic, 'general-test')

        self.assertIn('__consumer_offsets', unresolved_topics.keys())
        self.assertEqual(unresolved_topics.get('__consumer_offsets').topic, '__consumer_offsets')

#
#      Copyright (C) 2021 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
from unittest import TestCase

from axualclient.discovery import BOOTSTRAP_SERVERS_KEY
from axualclient.util import first_of_string_list, filter_axual_configuration, calculate_producer_switch_timeout


class Test(TestCase):

    def test_filter_axual_configuration_empty(self):
        arg = {}
        self.assertEqual({}, filter_axual_configuration(arg))

    def test_filter_axual_configuration_one_dropped(self):
        arg = {'tenant': 'some tenant'}
        self.assertEqual({}, filter_axual_configuration(arg))

    def test_filter_axual_configuration_multiple_dropped(self):
        arg = {'tenant': 'a tenant', 'instance': 'an instance', BOOTSTRAP_SERVERS_KEY: 'stays'}
        self.assertEqual({BOOTSTRAP_SERVERS_KEY: 'stays'}, filter_axual_configuration(arg))
        self.assertIn('tenant', arg, msg='Should not remove alter the parameter contents')
        self.assertDictEqual(arg, {'tenant': 'a tenant', 'instance': 'an instance', BOOTSTRAP_SERVERS_KEY: 'stays'},
                             msg='Should not remove alter the parameter contents')

    def test_first_of_string_list(self):
        arg = 'https://sr-1.axual.nl:8081,https://sr-2.axual.nl:8081,https://sr-3.axual.nl:8081'
        self.assertEqual('https://sr-1.axual.nl:8081', first_of_string_list(arg))

    def test_first_of_string_list_no_comma(self):
        arg = 'https://sr.axual.nl:8081'
        self.assertEqual('https://sr.axual.nl:8081', first_of_string_list(arg))

    def test_calculate_producer_switch_timeout_not_keeping_order(self):
        self.assertEqual(0., calculate_producer_switch_timeout(False, 0, 0, datetime.datetime.utcnow()))

    def test_calculate_producer_switch_timeout_keeping_order(self):
        ten_minutes_ago = datetime.datetime.utcnow() - datetime.timedelta(seconds=0)
        timeout = calculate_producer_switch_timeout(True, 10000, 1, ten_minutes_ago)
        self.assertAlmostEqual(10., timeout, delta=1.0)

    def test_calculate_producer_switch_timeout_keeping_order_half_way(self):
        ten_minutes_ago = datetime.datetime.utcnow() - datetime.timedelta(seconds=5)
        timeout = calculate_producer_switch_timeout(True, 10000, 1, ten_minutes_ago)
        self.assertAlmostEqual(5., timeout, delta=1.0)
